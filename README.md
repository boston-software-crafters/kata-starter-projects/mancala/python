# Mancala

An ancient game of pits and stones

## How to play the game ([video](https://youtu.be/OX7rj93m6o8))
* The Mancala board is made up of two rows of six pockets each (also called “holes,” or “pits”)
* Four stones are placed in each of the 12 pockets
* Each player has a “store” (also called a “Mancala”) to their right side of the Mancala board.
* The object of the game is to collect the most pieces by the end of the game.
* The game begins with one player picking up all of the pieces in any one of the pockets on his/her side.
* Moving counter-clockwise, the player deposits one of the stones in each pocket until the stones run out.
* If you run into your own Mancala (store), deposit one piece in it. If you run into your opponent's Mancala, skip it and continue moving to the next pocket.
* If the last piece you drop is in your own Mancala, you take another turn.
* If the last piece you drop is in an empty pocket on your side, you capture that piece and any pieces in the pocket directly 
* Always place all captured pieces in your Mancala (store).
* The game ends when all six pockets on one side of the Mancala board are empty.
* The player who still has pieces on his/her side of the board when the game ends captures all of those pieces.
* Count all the pieces in each Mancala. The winner is the player with the most pieces.

### Try it
[play mancala free online](https://mancala.playdrift.com/) 


## The Kata
The goal of the kata is simple: Implement the rules of mancala

## Project setup  
Install the requirements:  
`pip install -r requirements`  

## Testing
Run the tests:  
`pytest`  

Run the tests with code coverage report:  
`pytest --cov=kata .`  

Run the tests in watch mode:  
`ptw --runner "pytest --cov=kata"`  



## Tips & Ideas
The board could be represented as an array of integers:  
0-6 are the holes on player one's side of the board  
8-13 are the holes on player two's side of the board  
7 is player one's mancala  
14 is player two's mancala   

So the starting board would look like this:  
[4,4,4,4,4,4,0,4,4,4,4,4,4,0]  
There are 4 stones in each of player one's holes and 4 stones in each of player two's holes  

Knowing the state of the game also requires knowing who's turn it is:  
(**1**, [4,4,4,4,4,4,0,4,4,4,4,4,4,0])   player **one**'s turn  
(**2**, [4,0,5,5,5,5,0,4,4,4,4,4,4,0])   player **two**'s turn  

To make a move, indicate the store you want to distribute from and a game state  
move(1, (1,[4,4,4,4,4,4,0,4,4,4,4,4,4,0])) -> (2,[4,0,5,5,5,5,0,4,4,4,4,4,4,0])  
The function should return the new game state  


Test Cases:  
move(7, (2,[4,4,4,4,4,4,0,4,4,4,4,4,4,0])) -> (1,[4,4,4,4,4,4,0,0,5,5,5,5,4,0])   
move(1 (2, [4,4,4,4,4,4,0,4,4,4,4,4,4,0])) ->  (1, [4,4,0,5,5,5,1,4,4,4,4,4,4,0])   
move(12, (2,[4,4,4,4,4,4,0,4,4,4,4,4,4,0])) -> (1,[5,5,5,5,4,4,0,4,4,4,4,4,0,1])   


## Resources and References:
* [How to play mancala video](https://youtu.be/OX7rj93m6o8)
* [play mancala free online](https://mancala.playdrift.com/)
* [the wikipedia article](https://en.wikipedia.org/wiki/Mancala)



![Mancala board](https://upload.wikimedia.org/wikipedia/commons/4/42/Thai-mancala-board-possibly-main-chakot-or-mak-khom.jpg)
